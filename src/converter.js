/**
 * Padding hex component if necessary.
 * Hex component representation requires
 * two hexadecimal characters.
 * @param {string} comp 
 * @returns {string} two hexadecimal characters.
 */
const pad = (comp) => {
    let padded = comp.length == 2 ? comp : "0" + comp;
    return padded;
};

/**
 * RGB-to-HEX conversion
 * @param {number} r RED 0-255
 * @param {number} g GREEN 0-255
 * @param {number} b BLUE 0-255
 * @returns {string} in hex color format, e.g., "#00ff00" (green)
 */
export const rgb_to_hex = (r, g, b) => {
    const HEX_RED = r.toString(16);
    const HEX_GREEN = g.toString(16);
    const HEX_BLUE = b.toString(16);
    return "#" + pad(HEX_RED) + pad(HEX_GREEN) + pad(HEX_BLUE);
};

export const hex_to_rgb = (hex) => {
    hex = hex.replace('#', '');

var r = parseInt(hex.substring(0, 2), 16);
var g = parseInt(hex.substring(2, 4), 16);
var b = parseInt(hex.substring(4, 6), 16);

    return {
        r: r,
        g: g,
        b: b
    };
};